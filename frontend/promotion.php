<!DOCTYPE html>
<html lang="en">
	<head>
		<title>unidry-promotion</title>
		<meta name="vỉewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" type="text/css" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
		<script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
    <body>
    	<header>
			<div class="container-fluid baner">
				<nav class="navbar navbar-expand-lg navbar-light" style="background-color: rgba(255,255,255,0);">
		            <a class="navbar-brand">
		              <img src="image/signal.png">
		            </a>
		            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		              <span class="navbar-toggler-icon"></span>
		            </button>

		            <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
		              <ul class="navbar-nav list">
		                <li class="nav-item active">
		                  <a class="nav-link imp" href="#"> Trang chủ</a>
		                </li>

		                <li class="nav-item">
		                  <a class="nav-link inside" href="#">Thể lệ</a>
		                </li>

		                <li class="nav-item">
		                  <a class="nav-link inside" href="#">Tham gia ngay</a>
		                </li>

		                <li class="nav-item">
		                  <a class="nav-link inside" href="#">Chia sẻ kinh nghiệm</a>
		                </li> 

		                <li class="nav-item">
		                  <a class="nav-link inside" href="#">Bài dự thi</a>
		                </li>
		                
		                <li class="nav-item icon">
		                  <a class="nav-link icon-1" href="#"><i class="fa fa-facebook" aria-hidden="true"></i>
		                  </a>  
		                </li>

		                <li class="nav-item icon">
		                  <a class="nav-link icon-2" href="#"><i class="fa fa-twitter" aria-hidden="true"></i>
		                  </a>  
		                </li>

		                <li class="nav-item icon">
		                  <a class="nav-link icon-3" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i>
		                  </a>  
		                </li>

		                <li class="nav-item icon">
		                  <a class="nav-link icon-4" href="#"><i class="fa fa-instagram" aria-hidden="true"></i>
		                  </a>  
		                </li>
		                  <li class="nav-item sign-in">
		                    <a class="nav-link" href="#">Đăng kí</a>
		                    <a class="nav-link" href="#">Đăng nhập</a>
		              </li>
					        </ul>
					    </div>
					</nav>
				</div>
		</header>
		<div class="baner-infor">
				<img class="info" src="./image/information.png" alt="">
				<!--information about sale -->
		</div>
		<section class="container-fluid sale">
				<div class="row">
					<div class="col-md-5 left">
						<img src="./image/sale-50.png">
					</div>
					<div class="col-md-7 right">
						<h2 class="title">1000 ngày</h2>
						<span class="sup-title">mặc tã của bé</span>
							<p class="grow">Chúc mừng bé Lan Anh đạt 03 tháng tuổi<br><br>Bé đã lên size tã mới. Bé nhận được gói tã miến phí từ Unidry</p>
						<!--admit de nhận quà-->
						<a href="#" role="button" class="btn btn recieve">xác nhận nhận quà</a>
					</div>
				</div>

		</section>
		<!-- siêu khuyến mãi -->
		<section class="container-fluid super-sale">
				<div class="cloud">
					<img src="image/cloud.png">
				</div>
				<div class="row">
					<div class="col-md-4 decrease">
						<img src="image/sale-30.png">
					</div>
					<div class="col-md-4 decrease">
						<img src="image/sale-10.png">
					</div>
					<div class="col-md-4 decrease">
						<img src="image/sale-60.png">
					</div>
				</div>
		</section>
		<footer>
		 		<div class="container last-page">
			        <div class="row">
			          <div class="col-md-4 col-sm-4">
			            <h2 class="title"><span style="color:#696969;">Giới thiệu</span> về chúng tôi</h2>
			              <img class="straight" src="image/straight.png">
			                <div class="content-left">
			                  <p> CÔNG TY TNHH TAISUN VIỆT NAM</p>
			                  <p><i class="fa fa-map-marker" aria-hidden="true"></i>   Lô A 1-6, Đường N5 KCN Tây Bắc Củ Chi, Hồ Chí Minh, Việt Nam.</p>
			                  <p><i class="fa fa-phone-square" aria-hidden="true"></i>  (84-8) 3790 8681 ~5</p>
			                  <p><i class="fa fa-phone-square" aria-hidden="true"></i>  (84-8) 3790 8687 </p>
			                  <p><i class="fa fa-envelope-o" aria-hidden="true"></i> <span style="color: #ffb600">chamsoccanha.com</span></p>
			                </div>
			              </div>

			            <div class="col-md-4 col-sm-4">
			            <h2 class="title"><span style="color: #696969;">Văn phòng</span> kinh doanh</h2>
			              <img class="straight" src="image/straight.png">
			                <div class="content-left">
			                  <p> VĂN PHÒNG KINH DOANH TP. HỒ CHÍ MINH</p>
			                  <p><i class="fa fa-map-marker" aria-hidden="true"></i>   Số 11, Đường Nguyễn Bính, Phú Mỹ Hưng, Quận 7. Tp. Hồ Chí Minh.</p>
			                  <p><i class="fa fa-phone-square" aria-hidden="true"></i>  (84-8) 5 4123 136 ~8</p>
			                  <p><i class="fa fa-phone-square" aria-hidden="true"></i>  (84-8) 5 4123 139 </p>
			                  <p><i class="fa fa-envelope-o" aria-hidden="true"></i> <span style="color: #ffb600">chamsoccanha.com</span></p>
			                </div>
			          </div>

			          <div class="col-md-4 col-sm-4">
			            <h2 class="title"> Hình ảnh</h2>
			              <img class="straight" src="image/straight.png">
			            <div class="row">
			              <div class="col-sm-4">
			                <div class="wrap-img">
			                  <img src="image/footer-picture-1.png">
			                  <img src="image/footer-picture-4.png">
			                </div>
			              </div>

			              <div class="col-sm-4">
			                <div class="wrap-img">
			                  <img src="image/footer-picture-2.png">
			                  <img src="image/footer-picture-3.png">
			                </div>
			              </div>

			              <div class="col-sm-4">
			                <div class="wrap-img">
			                  <img src="image/footer-picture-6.png">
			                  <img src="image/footer-picture-5.png">
			                </div>
			              </div>
			            </div>
			          </div>
			        </div>
			      </div>
			<div class="leg-foot">
			    <a href="#" role="button" class="btn btn-primary"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
			</div>
		      <div class="container-fluid footer-main">
		        <nav class="navbar navbar-expand-lg navbar">
		              <a class="navbar-brand" href="#">
		                <i class="fa fa-copyright" aria-hidden="true"></i> <span style="color: orange"> Unidry.</span> All Rights Reserved. </a>
		              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		                <span class="navbar-toggler-icon"></span>
		              </button>

		              <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
		                <ul class="navbar-nav slimz">
		                  <li class="nav-item active">
		                    <a class="nav-link" href="#"><span style="color: orange">Trang Chủ</span> </a>
		                  </li>
		                  <li class="nav-item">
		                    <a class="nav-link" href="#">Thể lệ</a>
		                  </li>
		                  <li class="nav-item">
		                    <a class="nav-link " href="#">Tham gia ngay</a>
		                  </li>
		                  <li class="nav-item">
		                    <a class="nav-link " href="#">Chia sẻ kinh nghiệm</a>
		                  </li>
		                  <li class="nav-item">
		                    <a class="nav-link " href="#">Bài dự thi</a>
		                  </li>
		            </ul>
		          </div>
		       </nav>
		    </div>
		   </footer>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</body>