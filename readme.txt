The first setup :

- setup composer, link https://getcomposer.org/download/
- set up node js latest version , link https://nodejs.org/en/
Continue :
- Open command line (Window + R, typing cmd)
- CD to project folder
- to setup library for laravel, typing  $ composer install 
- setup global bower $npm install -g -bower
- setup bower $bower install


For frontend developer
- setup gulp $npm install
- setup global gulp $npm install -g gulp
- setup browser-sync $npm install browser-sync
- setup gulp-sass $npm install gulp-sass
- setup gulp-concat $npm install gulp-concat
- setup gulp-rename $npm install gulp-rename
- setup $npm install gulp

